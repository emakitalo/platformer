# platformer

Pure canvas platformer with express  

## Installation

Requirements
  - [Node.js](https://nodejs.org/en/download/)

```
git clone https://gitlab.com/emakitalo/platformer  
cd platformer  
npm install  
npm run dev
```