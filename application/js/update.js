"use strict";

import { inArray } from "./utilities.js";
import { updateInput } from "./input.js";
import actionHandlers from "./actions.js";

function updateBackground(game) {
  const player = game.activeLevel.player;
  if (
    inArray(["moveRight", "moveLeft"], player.state) &&
    game.activeLevel.onEdge
  ) {
    game.activeLevel.stage.background.forEach(bg => {
      const w = game.assets.images[bg.image][2][0];
      bg.position[0] +=
        (game.activeLevel.player.velocity[0] *
          game.activeLevel.player.speedX *
          bg.z) /
        game.timeScale;
      bg.position[0] = bg.position[0].mod(w);
    });
    game.activeLevel.stage.foreground.forEach(fg => {
      const w = game.assets.images[bg.image][2][0];
      fg.position[0] +=
        (game.activeLevel.player.velocity[0] *
          game.activeLevel.player.speedX *
          fg.z) /
        game.timeScale;
      fg.position[0] = fg.position[0].mod(w);
    });
  }
}

function removeInactiveObjects(game) {
  if (game.activeLevel.inactiveObjects.length > 0) {
    for (let i = 0; i < game.activeLevel.inactiveObjects.length; i++) {
      game.activeLevel.activeObjects.splice(
        game.activeLevel.inactiveObjects[i],
        1
      );
    }
    game.activeLevel.inactiveObjects = [];
  }
}

function updateGameObjects(game) {
  game.timeScale = game.deltaTime * game.speed;

  const renderOrder = [];
  const objs = game.activeLevel.activeObjects;

  for (let i = 0; i < objs.length; i++) {
    let obj = objs[i];
    const actions = obj.activeActions;
    if (actions.length > 0) {
      for (let j = 0; j < actions.length; j++) {
        actionHandlers[actions[j]](obj);
      }
    } else {
      obj.velocity[0] = 0;
      obj.velocity[1] = 0;
    }

    if (obj.type === "player") {
      if (
        !(
          (obj.position[0] < game.activeLevel.edgeOffset &&
            obj.direction[0] < 0) ||
          (obj.position[0] >
            game.width -
              (game.activeLevel.edgeOffset + obj.sprites.static.frameSize[0]) &&
            obj.direction[0] > 0)
        )
      ) {
        game.activeLevel.onEdge = false;
      } else {
        game.activeLevel.onEdge = true;
      }

      if (!game.activeLevel.onEdge)
        obj.position[0] += (obj.velocity[0] * obj.speedX) / game.timeScale;
    } else {
      if (game.activeLevel.onEdge)
        obj.position[0] +=
          (obj.velocity[0] * obj.speedX -
            game.activeLevel.player.velocity[0] *
              game.activeLevel.player.speedX) /
          game.timeScale;
      else obj.position[0] += (obj.velocity[0] * obj.speedX) / game.timeScale;
    }

    obj.position[1] += (obj.velocity[1] * obj.speedY) / game.timeScale;

    const sprite = obj.sprites[obj.state[obj.state.length - 1]];
    renderOrder.push([
      i,
      obj.ground ? obj.ground : obj.position[1],
      sprite.frameSize[1],
      sprite.ZOffset
    ]);
  }

  game.activeLevel.stage.renderOrder = renderOrder.sort((a, b) => {
    return a[1] + a[2] - b[1] - (b[2] - b[3]);
  });
}

function updateGameplay(game) {
  updateBackground(game);
  removeInactiveObjects(game);
  updateGameObjects(game);
}

function updateMenu(game) {}

const states = {
  gameplay: updateGameplay,
  menu: updateMenu
};

export function update(game) {
  updateInput(game);
  states[game.activeState](game);
}
