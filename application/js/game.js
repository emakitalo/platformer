"use strict";

import { initInput } from "./input.js";
import { initGui } from "./gui.js";
import { draw } from "./draw.js";
import { update } from "./update.js";

function start(game) {
  let updateId = setInterval(
    tick => {
      const time = tick();
      if (!game.lastTime) {
        game.lastTime = time + 1;
      }
      game.deltaTime = time - game.lastTime;
      if(game.active)
        update(game);
      game.lastTime = time;
    },
    1000 / game.ups,
    Date.now
  );

  function drawAnimation() {
    if(game.active)
      draw(game);
    window.requestAnimationFrame(drawAnimation);
  }

  let drawId = window.requestAnimationFrame(drawAnimation);
}

function init(game) {
  initGui(game);
  initInput(game);
  document.fonts.ready.then(() => {
    start(game);
  });
}

export default init;
