"use strict";

import { cloneEvent } from "./utilities.js";

function addTouches(e, game) {
  var touches = e.changedTouches;
  for (var i = 0; i < touches.length; i++) {
    const touch = cloneEvent(touches[i]);
    game.touches.push(touch);
  }
}

function updateTouches(e, game) {
  var touches = e.changedTouches;
  for (var i = 0; i < touches.length; i++) {
    const touch = cloneEvent(touches[i]);
    for(let j=0; j < game.touches.length; j++) {
      const prev = game.touches[j];
      if(touch.identifier === prev.identifier) {
        touch.dirX = touch.clientX - prev.clientX;
        touch.dirY = touch.clientY - prev.clientY;
        game.touches[j] = touch;
        game.touches[j].prevTouch = prev;
      }
    }
  }
}

function removeTouches(e, game) {
  var touches = e.changedTouches;
  for (var i = 0; i < touches.length; i++) {
    const touch = touches[i];
    for(let j=0; j < game.touches.length; j++) {
      const last = game.touches[j];
      if(touch.identifier === last.identifier) {
        game.touches.splice(i, 1);
      }
    }
  }
}

function handleStart(e, game, mouse = false) {
  e.preventDefault();
  if(!mouse)
    addTouches(e, game);
  else
    game.touches.push(e);
}

function handleEnd(e, game, mouse = false) {
  e.preventDefault();
  if(!mouse)
    removeTouches(e, game);
  else
    game.touches.pop();
  if(game.touches.length < 1) {
    game.inputs = [];
  }
  game.gui.activeMenu.activeNode = null;
  game.gui.activeMenu.update = true;
}

function handleCancel(e, game, mouse = false) {
  e.preventDefault();
  if(!mouse)
    removeTouches(e, game);
}

function handleMove(e, game, mouse = false) {
  e.preventDefault();
  if(!mouse)
    updateTouches(e, game);
}

export function initTouch(game) {
  const ctx = game.ctxGui.canvas;
  ctx.addEventListener(
    "touchstart",
    e => {
      handleStart(e, game);
    },
    false
  );

  ctx.addEventListener(
    "mousedown",
    e => {
      handleStart(e, game, true);
    },
    false
  );

  ctx.addEventListener(
    "touchend",
    e => {
      handleEnd(e, game);
    },
    false
  );


  ctx.addEventListener(
    "mouseup",
    e => {
      handleEnd(e, game, true);
    },
    false
  );

  ctx.addEventListener(
    "touchcancel",
    e => {
      handleCancel(e, game);
    },
    false
  );

  ctx.addEventListener(
    "touchmove",
    e => {
      handleMove(e, game);
    },
    false
  );
}
