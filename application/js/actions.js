"use strict";

import { keyToAction } from "./input.js";
import { clone } from "./utilities.js";
import "./cycle.js";

Number.prototype.mod = function (n) {
  return ((this % n) + n) % n;
};

const activeStates = {
  moveRight: ["moveRight", "moveUp", "moveDown", "jump"],
  moveLeft: ["moveLeft", "moveUp", "moveDown", "jump"],
  moveUp: ["moveUp", "moveRight", "moveLeft"],
  moveDown: ["moveDown", "moveRight", "moveLeft"],
  attack: ["attack", "jump", "moveDown", "moveUp", "moveRight", "moveLeft"],
  jump: ["jump", "attack", "moveRight", "moveLeft"]
};

function removeObject(obj) {
  obj.game.activeLevel.inactiveObjects.push(
    obj.game.activeLevel.activeObjects.indexOf(obj)
  );
}

function activeState(obj, states) {
  let active = true;
  obj.state.forEach(state => {
    if (!states.includes(state) && state !== "static") {
      active = false;
    }
  });

  if (!obj.state.includes(states[0])) active = false;

  return active;
}

function ifActive(obj, excludes, states) {
  let update = true;
  excludes.forEach(id => {
    if (obj.activeActions.includes(id)) {
      update = false;
    }
  });

  if (!update) return false;

  if (!activeState(obj, states)) return false;

  return true;
}

function removeSprite(obj, action) {
  if (obj.state.includes(action))
    obj.state.splice(obj.state.indexOf(action), 1);
}

function removeAction(obj, action) {
  if (obj.activeActions.includes(action))
    obj.activeActions.splice(obj.activeActions.indexOf(action), 1);
}

function removeSpriteAction(obj, action) {
  removeSprite(obj, action);
  removeAction(obj, action);
}

function setSprite(obj, action) {
  if (action in obj.sprites && !obj.state.includes(action))
    obj.state.push(action);
}

function setAction(obj, action) {
  setSprite(obj, action);
  if (!obj.activeActions.includes(action)) obj.activeActions.push(action);
}

function setActions(obj, actions) {
  actions.forEach(action => {
    setAction(obj, action);
  });
}

function checkDirection(sprite) {
  if (sprite.duration > 0) {
    return sprite.frame >= sprite.start + sprite.duration;
  } else {
    return sprite.frame <= sprite.stop;
  }
}

function updateAction(obj, sprite, action, func1 = () => {}, func2 = () => {}) {
  if (checkDirection(sprite) || !sprite.active) {
    const offset = sprite.offsetActive
      ? sprite.offset
        ? sprite.offset
        : 0
      : 0;
    sprite.frame = sprite.reversed
      ? sprite.stop - offset
      : sprite.start + offset;
    sprite.timer = 0;
    if (obj.state.indexOf(action) < 0) {
      sprite.frame = sprite.duration > 0 ? sprite.start : sprite.stop;
      sprite.active = false;
      removeAction(obj, action);
      if (sprite.pingpong) {
        sprite.pinged = false;
        sprite.ponged = false;
      }
      if (obj.state.length > 0) {
        const prevObj = obj.state[obj.state.length - 1];
        obj.sprites[prevObj].active = obj.noReset.includes(prevObj)
          ? true
          : false;
      }
      func1();
    } else {
      func2();
    }
  } else {
    sprite.frame += sprite.stop < sprite.start ? -1 : 1;
    sprite.timer = 0;
  }
}

function returnToRestPosition(obj, state, sprite) {
  const start = sprite.start;
  sprite.start = sprite.stop;
  sprite.stop = start;
  sprite.frame = sprite.start;
  sprite.duration = sprite.stop - sprite.start;
  sprite.reversed = !sprite.reversed;
  sprite.offsetActive = !sprite.offsetActive;
  if (sprite.ponged) {
    removeSprite(obj, state);
  } else {
    sprite.ponged = true;
  }
}

function idle(obj) {
  const state = "static";
  const sprite = obj.sprites[state];
  if (!sprite.active) {
    obj.velocity[0] = 0;
    sprite.active = true;
    sprite.frame = sprite.start;
  }
  if (sprite.timer > sprite.timerOffset) {
    updateAction(obj, sprite, state);
  }
  sprite.timer += obj.game.timeScale;
}

function jump(obj) {
  const state = "jump";
  const sprite = obj.sprites[state];
  const active = ifActive(obj, sprite.excludes, activeStates[state]);

  if (!sprite.active && active) {
    obj.direction[1] = 1;
    obj.velocity[1] = obj.direction[1] * obj.jumpForce;
    obj.ground = obj.position[1];
    sprite.active = true;
  }

  if (obj.ground < obj.position[1] + obj.velocity[1]) {
    sprite.active = false;
    obj.position[1] = obj.ground;
    obj.velocity[1] = 0;
    updateAction(obj, sprite, state);
  } else {
    if (obj.ground >= obj.position[1]) {
      obj.velocity[1] += obj.gravity + sprite.timer;
      sprite.timer += 0.01;
    }
  }
}

function clickJump(game) {
  if(!game.inputs.includes('jump'))
    document.dispatchEvent(new KeyboardEvent('keydown', {key: ' '}));
  else
    document.dispatchEvent(new KeyboardEvent('keyup', {key: ' '}));
}

function moveX(obj) {
  const state = "moveX";
  const sprite = obj.sprites[state];

  if (!sprite.active) {
    obj.velocity[0] = obj.direction[0];
    sprite.active = true;
  }

  if (sprite.timer > sprite.timerOffset * obj.speedX || !sprite.active) {
    updateAction(
      obj,
      sprite,
      state,
      () => {},
      () => {
        if (obj.position[0] < 0) obj.direction[0] = 1;
        if (obj.position[0] > obj.game.width - sprite.frameSize[0])
          obj.direction[0] = -1;
        sprite.active = false;
      }
    );
  } else {
    sprite.timer += obj.game.timeScale;
  }
}

function moveLeft(obj) {
  const state = "moveLeft";
  const sprite = obj.sprites[state];
  const active = ifActive(obj, sprite.excludes, activeStates[state]);

  if (!sprite.active && active) {
    obj.direction[0] = -1;
    obj.velocity[0] = obj.direction[0];
    sprite.active = true;
  }

  if (sprite.timer > sprite.timerOffset * obj.speedX || !sprite.active) {
    updateAction(
      obj,
      sprite,
      state,
      () => {
        obj.velocity[0] = !active ? 0 : obj.velocity[0];
      },
      () => {
        obj.velocity[0] = active ? obj.direction[0] : 0;
      }
    );
  } else {
    sprite.timer += obj.game.timeScale;
  }
}

function moveRight(obj) {
  const state = "moveRight";
  const sprite = obj.sprites[state];
  const active = ifActive(obj, sprite.excludes, activeStates[state]);

  if (!sprite.active && active) {
    obj.direction[0] = 1;
    obj.velocity[0] = obj.direction[0];
    sprite.active = true;
  }

  if (sprite.timer > sprite.timerOffset * obj.speedX || !sprite.active) {
    updateAction(
      obj,
      sprite,
      state,
      () => {
        obj.velocity[0] = !active ? 0 : obj.velocity[0];
      },
      () => {
        obj.velocity[0] = active ? obj.direction[0] : 0;
      }
    );
  } else {
    sprite.timer += obj.game.timeScale;
  }
}

function moveUp(obj) {
  const state = "moveUp";
  const sprite = obj.sprites[state];
  const active = ifActive(obj, sprite.excludes, activeStates[state]);
  const jump = obj.activeActions.includes("jump");

  if (!sprite.active && active) {
    obj.direction[1] = -1;
    obj.velocity[1] = obj.direction[1];
    sprite.active = true;
  }

  if (sprite.timer > sprite.timerOffset * obj.speedY || !sprite.active) {
    updateAction(
      obj,
      sprite,
      state,
      () => {
        obj.velocity[1] = !active ? 0 : obj.velocity[1];
      },
      () => {
        if (!jump) obj.velocity[1] = active ? obj.direction[1] : 0;
      }
    );
  } else {
    if (!jump) obj.ground = obj.position[1];
    sprite.timer += obj.game.timeScale;
  }
}

function moveDown(obj) {
  const state = "moveDown";
  const sprite = obj.sprites[state];
  const active = ifActive(obj, sprite.excludes, activeStates[state]);
  const jump = obj.activeActions.includes("jump");

  if (!sprite.active && active) {
    obj.direction[1] = 1;
    obj.velocity[1] = obj.direction[1];
    sprite.active = true;
  }

  if (sprite.timer > sprite.timerOffset * obj.speedY || !sprite.active) {
    updateAction(
      obj,
      sprite,
      state,
      () => {
        obj.velocity[1] = !active ? 0 : obj.velocity[1];
      },
      () => {
        if (!jump) obj.velocity[1] = active ? obj.direction[1] : 0;
      }
    );
  } else {
    if (!jump) obj.ground = obj.position[1];
    sprite.timer += obj.game.timeScale;
  }
}

function moveSineX(obj) {
  obj.speedX = Math.sin(obj.game.deltaTime).mapTo(-Math.PI, Math.PI, -1, 1);
}

function moveSineY(obj) {
  obj.speedY = Math.sin(Date.now() * 0.0005).mapTo(
    -Math.PI,
    Math.PI,
    -0.75,
    0.75
  );
}

function playSample(game, buffer) {
  const source = game.ctxAudio.createBufferSource();
  source.buffer = buffer;
  source.connect(game.volume);
  source.start();
}

function moveAmmo(obj) {
  const state = "static";
  const sprite = obj.sprites[state];
  if (sprite.timer > sprite.timerOffset) {
    sprite.frame = ((sprite.frame + 1) % sprite.stop) + 1;
  }

  obj.speedX += obj.speedX - obj.speedX * 0.99;
  sprite.timer += obj.game.timeScale;

  if (obj.position[0] < 0 || obj.position[0] > obj.game.width) {
    removeObject(obj);
  }

  if (checkHits(obj, ["enemy"], damageCollider)) {
    obj.game.activeLevel.player.score += 10;
    obj.game.gui.children.hud.children.score.value =
      obj.game.activeLevel.player.score;
    obj.game.gui.children.hud.update = true;
    playSample(obj.game, obj.game.assets.sounds[obj.sounds.hit]);
    removeObject(obj);
  }
}

function die(obj) {
  const state = "die";
  const sprite = obj.sprites[state];

  if (!sprite.active) {
    sprite.active = true;
    obj.active = false;
    sprite.pinged = true;
  }

  if (sprite.timer > sprite.timerOffset) {
    updateAction(
      obj,
      sprite,
      state,
      () => {
        if(obj.type === "player") {
          if(obj.lives > 1) {
            obj.lives--;
            obj.health = obj.maxHealth;
            obj.active = true;
            obj.game.gui.children.hud.children.lives.value = obj.lives;
            obj.game.gui.children.hud.children.health.value = obj.health;
            obj.game.gui.children.hud.update = true;
          }
          else {
            showMenu(obj.game);
            obj.game.gui.activeMenu.active = false;
            obj.game.gui.activeMenu = getNode(obj.game, ['gui','gameover']);
            obj.game.gui.activeMenu.active = true;
            obj.game.gui.activeMenu.draw = true;
          }
        }
      },
      () => {
        if (sprite.pinged) {
          returnToRestPosition(obj, state, sprite);
          obj.game.inputs = [];
        }
      }
    );
  } else {
    sprite.timer += obj.game.timeScale;
  }
}

function hit(obj) {
  const state = "hit";
  const sprite = obj.sprites[state];

  if (!sprite.active) {
    sprite.timerOffset = 1000 / (obj.game.ups * 0.5);
    sprite.active = true;
    obj.active = false;
  }

  if (sprite.timer > sprite.timerOffset) {
    removeSpriteAction(obj, state);
    sprite.timer = 0;
    sprite.active = false;
    obj.active = true;
  } else {
    sprite.timer += obj.game.timeScale;
  }
}

function shoot(obj, sprite) {
  const ammo = clone(obj.itemsActive.weapon);
  ammo.game = obj.game;
  ammo.position = clone(obj.position);
  ammo.position[0] += obj.direction[0] > 0 ? sprite.frameSize[0] - 4 : 0;
  ammo.position[1] += sprite.frameSize[1] * 0.5 - 8;
  ammo.direction[0] = clone(obj.direction[0]);
  ammo.velocity[0] += ammo.direction[0];
  ammo.sprites["static"].active = true;
  ammo.activeActions.push("moveAmmo");
  ammo.state.push("static");
  obj.game.activeLevel.activeObjects.push(ammo);
  playSample(obj.game, obj.game.assets.sounds[obj.sounds.shoot]);
}


function attack(obj) {
  const state = "attack";
  const sprite = obj.sprites[state];
  const active = ifActive(obj, sprite.excludes, activeStates[state]);

  if (!sprite.active && active) {
    sprite.active = true;
  }

  //const offset = !sprite.offsetActive && !sprite.reversed ? obj.game.ups / sprite.offsetFps / Math.abs(sprite.duration) : 0;
  //console.log(offset);
  if (sprite.timer > sprite.timerOffset || !sprite.active) {
    updateAction(
      obj,
      sprite,
      state,
      () => {},
      () => {
        if (obj.active && !sprite.reversed) {
          shoot(obj, sprite);
        }

        if (sprite.pinged) {
          returnToRestPosition(obj, state, sprite);
        }
      }
    );
  } else {
    sprite.timer += obj.game.timeScale;
  }
}

function clickAttack(game) {
  if(!game.inputs.includes('attack'))
    document.dispatchEvent(new KeyboardEvent('keydown', {key: 'x'}));
  else
    document.dispatchEvent(new KeyboardEvent('keyup', {key: 'x'}));
}

function getBoundingBox(obj, pad) {
  const ofs = obj.sprites[obj.state[obj.state.length - 1]].frameSize;
  const ol = obj.direction[0] > 0 ? pad.left : ofs[0] - pad.right;
  const or = obj.direction[0] > 0 ? pad.right : ofs[0] - pad.left;
  return [ol, pad.top, or, pad.bottom];
}

function checkBoxCollision(obj, col, pad1 = null, pad2 = null) {
  const obb = getBoundingBox(obj, pad1 ? pad1 : obj.boundingBox.default);
  const cbb = getBoundingBox(col, pad2 ? pad2 : col.boundingBox.default);
  const oy = obj.position[1];
  const cy = col.position[1];
  return (
    obj.position[0] + obb[0] < col.position[0] + cbb[2] &&
    obj.position[0] + obb[2] > col.position[0] + cbb[0] &&
    oy + obb[1] < cy + cbb[3] &&
    oy + obb[3] > cy + cbb[1]
  );
}

function checkPixelCollision(oOl, op, cOl, cp) {
  for (let i = 0; i < oOl.length; i += 2) {
    const ox = Math.round(op[0] + oOl[i]);
    const oy = Math.round(op[1] + oOl[i + 1]);
    for (let j = 0; j < cOl.length; j += 2) {
      const cx = Math.round(cp[0] + cOl[j]);
      const cy = Math.round(cp[1] + cOl[j + 1]);
      if (ox - cx <= 0 && oy - cy <= 0) return true;
    }
  }
  return false;
}

function checkHealth(obj) {
  if (obj.health <= 0) {
    if ("die" in obj.sprites) {
      obj.active = false;
      setAction(obj, "die");
    } else if (obj.type === "enemy") {
      obj.game.activeLevel.player.health += 5;
      obj.game.gui.children.hud.children.health.value =
        obj.game.activeLevel.player.health;
      obj.game.gui.children.hud.update = true;
      removeObject(obj);
    }
  } else {
    if ("hit" in obj.sprites) {
      setAction(obj, "hit");
    }
  }
}

function damageObject(obj, col, types, exclude = []) {
  let hit = false;
  if (types.includes(obj.type) && !exclude.includes(col.type) && obj !== col) {
    if (col.hitAreas) {
      col.hitAreas.forEach(name => {
        if (obj.boundingBox[name]) {
          if (checkBoxCollision(obj, col, obj.boundingBox[name])) {
            const d = obj.direction[0] > 0 ? 2 : 3;
            const sp = obj.game.assets.sprites;
            const o = obj.sprites[obj.state[obj.state.length - 1]];
            const os = sp[o.sprite][d][o.frame];
            const c = col.sprites[col.state[col.state.length - 1]];
            const cs = sp[c.sprite][d][c.frame];
            if (checkPixelCollision(os, obj.position, cs, col.position)) {
              obj.health -= col.damage;
              col.velocity[0] = 0.25;
              checkHealth(obj);
              checkHealth(col);
              hit = true;
            }
          }
        }
      });
    } else {
      if (checkBoxCollision(obj, col, obj.boundingBox.default)) {
        const d = obj.direction[0] > 0 ? 2 : 3;
        const sp = obj.game.assets.sprites;
        const o = obj.sprites[obj.state[obj.state.length - 1]];
        const os = sp[o.sprite][d][o.frame];
        const c = col.sprites[col.state[col.state.length - 1]];
        const cs = sp[c.sprite][d][c.frame];
        if (checkPixelCollision(os, obj.position, cs, col.position)) {
          obj.health -= col.damage;
          col.velocity[0] = 0.25;
          checkHealth(obj);
          checkHealth(col);
          hit = true;
        }
      }
    }
  }
  return hit;
}

function damageCollider(obj, col, types, exclude = []) {
  let hit = false;
  if (types.includes(col.type) && obj !== col) {
    if (obj.hitAreas) {
      obj.hitAreas.forEach(name => {
        if (col.boundingBox[name]) {
          if (
            checkBoxCollision(
              obj,
              col,
              obj.boundingBox.default,
              col.boundingBox[name]
            )
          ) {
            const d = obj.direction[0] > 0 ? 2 : 3;
            const sp = obj.game.assets.sprites;
            const o = obj.sprites[obj.state[obj.state.length - 1]];
            const os = sp[o.sprite][d][o.frame];
            const c = col.sprites[col.state[col.state.length - 1]];
            const cs = sp[c.sprite][d][c.frame];
            if (checkPixelCollision(os, obj.position, cs, col.position)) {
              col.health -= obj.damage;
              checkHealth(obj);
              checkHealth(col);
              hit = true;
            }
          }
        }
      });
    } else {
      if (
        checkBoxCollision(
          obj,
          col,
          obj.boundingBox.default,
          col.boundingBox.default
        )
      ) {
        const d = obj.direction[0] > 0 ? 2 : 3;
        const sp = obj.game.assets.sprites;
        const o = obj.sprites[obj.state[obj.state.length - 1]];
        const os = sp[o.sprite][d][o.frame];
        const c = col.sprites[col.state[col.state.length - 1]];
        const cs = sp[c.sprite][d][c.frame];
        if (checkPixelCollision(os, obj.position, cs, col.position)) {
          col.health -= obj.damage;
          checkHealth(obj);
          checkHealth(col);
          hit = true;
        }
      }
    }
  }
  return hit;
}

function checkHits(obj, types, func, exclude = []) {
  let hit = false;
  for (let i = 0; i < obj.game.activeLevel.activeObjects.length; i++) {
    hit = func(obj, obj.game.activeLevel.activeObjects[i], types, exclude);
    if (hit) break;
  }
  return hit;
}

function checkEnemyHits(obj) {
  obj.active
    ? checkHits(obj, ["player"], damageObject, ["ammo"])
      ? (() => {
          obj.game.gui.children.hud.children.health.value = obj.health;
          obj.game.gui.children.hud.update = true;
        })()
      : false
    : false;
}

function spawn(game, name, actions, params = {}) {
  let obj = clone(game.assets.gameobjects[name]);
  obj.game = game;
  setActions(obj, actions);
  Object.keys(params).forEach(param => {
    obj[param] = params[param];
  });
  game.activeLevel.activeObjects.push(obj);
}

function clickSpawn(game) {
  document.dispatchEvent(new KeyboardEvent('keydown', {key: 'a'}));
}

function checkLevelCollision(obj) {
  const od = obj.direction[0] > 0;
  const d = od ? 2 : 3;
  const state = obj.state[obj.state.length - 1];
  const spr = obj.sprites[state];
  const ol = obj.game.assets.sprites[spr.sprite][d];
  const lvl = obj.game.activeLevel;
  const bgs = lvl.stage.background;

  Object.keys(bgs).forEach(idx => {
    const bg = bgs[idx];
    if (bg.collide) {
      const w = obj.game.assets.images[bg.image][2][0];
      const ox = Math.round(
        obj.position[0] + spr.frameSize[0] * 0.5 + bg.position[0]
      ).mod(w);
      const oy = Math.round(
        obj.position[1] + spr.frameSize[1] + bg.position[1]
      );
      const bgol = obj.game.assets.images[bg.image][1];
      for (let i = 0; i < bgol.length; i += 2) {
        const x = Math.round(bgol[i]);
        let n = i + 3;
        let p = i - 1;
        if (i === bgol.length - 1) {
          n = 1;
          p = bgol.length - 1;
        }
        const ny = od ? Math.round(bgol[n]) : Math.round(bgol[p]);
        if (oy < ny + 10 && ox === x && !obj.activeActions.includes("jump")) {
          obj.position[1] = ny - spr.frameSize[1] + 10;
          obj.velocity[1] = 0;
          obj.direction[1] = 0;
          break;
        }
      }
    }
  });
}

function showMenu(game) {
  if (game.activeLevel) {
    const menu = game.activeState !== "menu";
    game.activeState = !menu ? "gameplay" : "menu";
    game.activeLevel.active = menu ? false : true;
    game.activeLevel.clear = menu ? true : false;
    game.gui.activeMenu.active = false;
    game.gui.activeMenu.clear = true;
    game.gui.activeMenu = menu ? game.gui.children.menu : game.gui.children.hud;
    game.gui.activeMenu.active = true;
    game.gui.activeMenu.update = true;
    game.gui.activeMenu.activeNode = null;
    game.touches = [];
  }
}

function moveInMenu(game, dir) {
  const activeNode = game.gui.activeMenu.activeNode;
  const nodes = game.gui.activeMenu.children;
  const values = Object.values(nodes);
  const nodeId = values.indexOf(activeNode);
  let newNode = game.gui.activeMenu.activeNode;
  for (let i = 1; i < values.length + 1; i++) {
    const id = (nodeId + i * (dir[0] > 0 ? 1 : -1)).mod(values.length);
    if (values[id].active) {
      newNode = values[id];
      break;
    }
  }
  game.gui.activeMenu.activeNode = newNode;
  game.gui.activeMenu.activeNode.parent.draw = true;
}

function openParentMenu(game) {
  const node = game.gui.activeMenu.parent.parent;
  if (node && node.parent) {
    game.ctxGui.clearRect(0, 0, game.width, game.height);
    game.gui.activeMenu.active = false;
    game.gui.activeMenu = node;
    game.gui.activeMenu.active = true;
    game.gui.activeMenu.draw = true;
  }
}

function getNode(tree, names) {
  const name = names.shift();
  const node = tree[name];
  if (names.length > 0 && node) return getNode(node.children, names);
  else return node;
}

function openSubMenu(game) {
  const node = game.gui.activeMenu.activeNode;
  if (typeof node.children === "object") {
    game.ctxGui.clearRect(0, 0, game.width, game.height);
    game.gui.activeMenu.active = false;
    game.gui.activeMenu = node.children[node.name];
  } else {
    const child = node.children.split(".");
    game.ctxGui.clearRect(0, 0, game.width, game.height);
    game.gui.activeMenu.active = false;
    game.gui.activeMenu = getNode(game, child);
  }
  const activeNode =
    game.gui.activeMenu.children[Object.keys(game.gui.activeMenu.children)[0]];
  activeNode.active ? (game.gui.activeMenu.activeNode = activeNode) : null;
  game.gui.activeMenu.active = true;
  game.gui.activeMenu.draw = true;
}

function changeVolume(game, dir) {
  const volume = game.gui.activeMenu.activeNode.value + dir;
  if (volume <= 100 && volume >= 0) {
    game.volume.gain.value = volume / 100;
    game.gui.activeMenu.activeNode.value = volume;
    game.gui.activeMenu.activeNode.parent.draw = true;
  }
}

function newGame(game) {
  game.levels["lvl_001"].activeObjects = [];
  delete game.activeLevel;
  const name = game.playerObjName;
  const player = clone(game.assets.gameobjects[name]);
  player.name = name;
  player.type = "player";
  player.lives = 1;
  player.direction[0] = 1;
  player.position[0] = 20;
  player.position[1] = game.ctx.canvas.height / 2;
  player.health = player.maxHealth;
  player.score = 0;
  player.state.push("static");
  player.activeActions.push("checkEnemyHits");
  player.items.weapons["ammo_001"] = game.assets.gameobjects["ammo_001"];
  player.itemsActive.weapon = player.items.weapons.ammo_001;
  player.game = game;

  game.activeLevel = game.levels["lvl_001"];
  game.activeLevel.player = player;
  game.activeLevel.activeObjects.push(player);
  game.gui.children.hud.activeNode = game.gui.children.hud.children.title;
  game.gui.children.hud.children.lives.value = player.lives;
  game.gui.children.hud.children.score.value = player.score;
  game.gui.children.hud.children.health.value = player.health;
  game.gui.children.hud.update = true;

  showMenu(game);
}

function resumeGame(game) {
  showMenu(game);
}

function saveGame(game) {
  localStorage.setItem("save", JSON.stringify(JSON.decycle(game.activeLevel)));
  showMenu(game);
}

function loadGame(game) {
  delete game.activeLevel;
  const save = JSON.retrocycle(JSON.parse(localStorage.getItem("save")));
  save.activeObjects.forEach(obj => {
    obj.game = game;
  });
  game.gui.children.hud.children.lives.value = save.player.lives;
  game.gui.children.hud.children.score.value = save.player.score;
  game.gui.children.hud.children.health.value = save.player.health;
  game.gui.children.hud.update = true;
  game.activeLevel = save;
  showMenu(game);
}

function pause(game) {
  game.active = !game.active;
}

const actionHandlers = {
  static: idle,
  spawn: spawn,
  clickSpawn: clickSpawn,
  jump: jump,
  clickJump: clickJump,
  attack: attack,
  clickAttack: clickAttack,
  moveLeft: moveLeft,
  moveRight: moveRight,
  moveUp: moveUp,
  moveX: moveX,
  moveSineX: moveSineX,
  moveSineY: moveSineY,
  moveDown: moveDown,
  moveAmmo: moveAmmo,
  die: die,
  hit: hit,
  removeObject: removeObject,
  checkEnemyHits: checkEnemyHits,
  checkLevelCollision: checkLevelCollision,
  showMenu: showMenu,
  moveInMenu: moveInMenu,
  openSubMenu: openSubMenu,
  openParentMenu: openParentMenu,
  changeVolume: changeVolume,
  newGame: newGame,
  resumeGame: resumeGame,
  saveGame: saveGame,
  loadGame: loadGame,
  pause: pause
};

export default actionHandlers;
