"use strict";

export function getPixelId(x, y, w) {
  return y * (w * 4) + x * 4;
}

export function getData(ctx, img, frame, size) {
  ctx.canvas.width = size[0];
  ctx.canvas.height = size[1];
  ctx.drawImage(
    img,
    frame[0],
    frame[1],
    size[0],
    size[1],
    0,
    0,
    size[0],
    size[1]
  );
  const data = ctx.getImageData(0, 0, size[0], size[1]);
  return data;
}

export function getOutline(data, dim, c = 3, a = 0) {
  let ol = [];

  for (let x = 0; x < dim[0]; x++) {
    for (let y = 0; y < dim[1] - 1; y++) {
      const p = data[getPixelId(x, y, dim[0]) + c];
      const np = data[getPixelId(x, y + 1, dim[0]) + c];
      if (p !== np) {
        const o = np === a ? 0 : 1;
        const idy = (y + o) * dim[0] + x;
        ol[idy] = true;
      } else if (y === dim[1] - 2 && np !== a) {
        const idy = (y + 1) * dim[0] + x;
        ol[idy] = true;
      }
    }
  }

  for (let x = 0; x < dim[1] - 1; x++) {
    for (let y = 0; y < dim[0]; y++) {
      const p = data[getPixelId(y, x, dim[0]) + c];
      const np = data[getPixelId(y + 1, x, dim[0]) + c];
      if (p !== np) {
        const o = np === a ? 0 : 1;
        const idx = x * dim[0] + (y + o);
        ol[idx] = true;
      } else if (y === dim[0] - 2 && np !== a) {
        const idy = x * dim[0] + (y + 1);
        ol[idy] = true;
      }
    }
  }

  let olOut = [];
  for (let x = 0; x < dim[0]; x++) {
    for (let y = 0; y < dim[1]; y++) {
      const idx = y * dim[0] + x;
      if (ol[idx]) {
        olOut.push(x);
        olOut.push(y);
      }
    }
  }

  return olOut;
}

export function getOutline2(data, dim, b = true, ch = 3, a = 0) {
  let ol = [];
  let test = new Array(dim[0] * dim[1]).fill(false);
  for (let x = 1; x < dim[0] - 1; x++) {
    for (let y = 1; y < dim[1] - 1; y++) {
      const c = data[getPixelId(x, y, dim[0]) + ch];
      for (let px = -1; px < 2; px++) {
        for (let py = -1; py < 2; py++) {
          const p = data[getPixelId(x + px, y + py, dim[0]) + ch];
          if (p === a && c !== a) {
            const idx = y * dim[0] + x;
            if (!test[idx]) {
              ol.push(x);
              ol.push(y);
              test[idx] = true;
            }
          }
          if (
            (x + px < 1 ||
              x + px > dim[0] - 2 ||
              y + py < 1 ||
              y + py > dim[1] - 2) &&
            b
          ) {
            if (p !== a) {
              const idx = (y + py) * dim[0] + x + px;
              if (!test[idx]) {
                ol.push(x + px);
                ol.push(y + py);
                test[idx] = true;
              }
            }
          }
        }
      }
    }
  }
  return ol;
}

export function lineIntersection(x1, y1, x2, y2, x3, y3, x4, y4) {
  // calculate the distance to intersection point
  const uA =
    ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) /
    ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
  const uB =
    ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) /
    ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));

  // if uA and uB are between 0-1, lines are colliding
  if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
    return [x1 + uA * (x2 - x1), y1 + uA * (y2 - y1)];
  }
  return null;
}

export function linesTouching(x1, y1, x2, y2, x3, y3, x4, y4) {
  const denominator = (x2 - x1) * (y4 - y3) - (y2 - y1) * (x4 - x3);
  const numerator1 = (y1 - y3) * (x4 - x3) - (x1 - x3) * (y4 - y3);
  const numerator2 = (y1 - y3) * (x2 - x1) - (x1 - x3) * (y2 - y1);

  // Detect coincident lines (has a problem, read below)
  if (denominator == 0) return numerator1 == 0 && numerator2 == 0;

  const r = numerator1 / denominator;
  const s = numerator2 / denominator;

  return r >= 0 && r <= 1 && s >= 0 && s <= 1;
}

export function createBoundingBox(data) {
  const x = [];
  const y = [];
  data.points.forEach(pt => {
    x.push(pt[0]);
    y.push(pt[1]);
  });
  x.sort();
  y.sort();
  return [x.shift(1), y.shift(1), x.pop(), y.pop()];
}

export function inObject(keys, obj) {
  let state = false;
  keys.forEach(key => {
    if (key in obj) state = true;
  });
  return state;
}

export function inArray(keys, array) {
  let state = false;
  keys.forEach(key => {
    if (array.includes(key)) state = true;
  });
  return state;
}

export var tabActive = (function () {
  var stateKey,
    eventKey,
    keys = {
      hidden: "visibilitychange",
      webkitHidden: "webkitvisibilitychange",
      mozHidden: "mozvisibilitychange",
      msHidden: "msvisibilitychange"
    };
  for (stateKey in keys) {
    if (stateKey in document) {
      eventKey = keys[stateKey];
      break;
    }
  }
  return function (c) {
    if (c) document.addEventListener(eventKey, c);
    return !document[stateKey];
  };
})();

export function clone(data, refs) {
  return JSON.parse(JSON.stringify(data));
}

export function getFramePosition(direction, sprite) {
  let y = Math.floor(sprite.frame / sprite.columns);
  let x =
    direction < 0
      ? sprite.duration !== 0
        ? sprite.columns - (sprite.frame - y * sprite.columns) - 1
        : sprite.frame - y * sprite.columns
      : sprite.duration === 0
      ? sprite.columns - (sprite.frame - y * sprite.columns) - 1
      : sprite.frame - y * sprite.columns;
  if (sprite.vertical) {
    x = Math.floor(sprite.frame / sprite.rows);
    y = sprite.frame - x * sprite.rows;
  }
  return [x * sprite.frameSize[0], y * sprite.frameSize[1]];
}

export function cloneEvent(e) {
  const clone = {};
  for (let val in e) {
    clone[val] = e[val];
  }
  return clone;
}

export function getJSON(url) {
  return fetch(url).then(r => {
    if (r.status) {
      return r.json();
    } else {
      throw new Error(r.status);
    }
  });
}

export function getJSONs(obj, path) {
  const jsons = [];
  Object.keys(obj).forEach(key => {
    jsons.push(getJSON(path + key + ".json"));
  });
  return Promise.all(jsons);
}

export function getText(url) {
  return fetch(url).then(r => {
    if (r.status) {
      return r.text();
    } else {
      throw new Error(r.status);
    }
  });
}

export function getTexts(arr, path) {
  const texts = [];
  arr.forEach(key => {
    console.log(path + key);
    texts.push(getText(path + key));
  });
  return Promise.all(texts);
}

export function getImage(url, func = img => {}) {
  return fetch(url)
    .then(r => r.blob())
    .then(r => {
      let img = new Image();
      img.onload = () => {
        func(img);
      };
      img.src = window.URL.createObjectURL(r);
      return img;
    });
}

export function getImages(obj, path, ext) {
  const images = [];
  Object.keys(obj).forEach(key => {
    images.push(getImage(path + key + ext));
  });
  return Promise.all(images);
}

export function getAudio(url) {
  return fetch(url).then(r => {
    return r.arrayBuffer();
  });
}

export function getAudios(obj, path) {
  const audios = [];
  Object.keys(obj).forEach(key => {
    audios.push(getImage(path + key + ".mp3"));
  });
  return Promise.all(audios);
}

export function wrapText(ctx, text, x, y, maxWidth, fontSize, fontFace) {
  var words = text.split(" ");
  var line = "";
  var lineHeight = fontSize * 1.286; // a good approx for 10-18px sizes

  ctx.font = fontSize + " " + fontFace;
  ctx.textBaseline = "top";

  for (var n = 0; n < words.length; n++) {
    var testLine = line + words[n] + " ";
    var metrics = ctx.measureText(testLine);
    var testWidth = metrics.width;
    if (testWidth > maxWidth) {
      ctx.fillText(line, x, y);
      if (n < words.length - 1) {
        line = words[n] + " ";
        y += lineHeight;
      }
    } else {
      line = testLine;
    }
  }
  ctx.fillText(line, x, y);
}

export function resize(e) {
  const w = window.innerWidth;
  const h = window.innerHeight;
  const a = w / h;
  const cw = e.offsetWidth;
  const ch = e.offsetHeight;
  const ca = cw / ch;
  e.width = cw;
  e.height = ch;
  if (w > h && a > ca) {
    e.style.transform = "scale(" + h / e.offsetHeight + ")";
  } else if(w > cw) {
    e.style.transform = "scale(" + w / e.offsetWidth + ")";
  }
}
