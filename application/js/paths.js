"use strict";

const assetsPath = "assets/";
const gamePath = assetsPath + "game/";
const menusPath = gamePath + "menus/";
const levelsPath = gamePath + "levels/";
const gameobjectsPath = gamePath + "gameobjects/";
const spritesPath = gamePath + "sprites/";
const imagesPath = gamePath + "images/";
const shadersPath = gamePath + "shaders/";
const soundsPath = gamePath + "sounds/";

export default {
  assets: assetsPath,
  game: gamePath,
  menus: menusPath,
  gameobjects: gameobjectsPath,
  sprites: spritesPath,
  images: imagesPath,
  levels: levelsPath,
  shaders: shadersPath,
  sounds: soundsPath
};
