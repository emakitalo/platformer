"use strict";

import { clone, vis } from "./utilities.js";
import actionHandlers from "./actions.js";

export function drawNode(ctx, node) {
  const w = node.dimensions[0];
  const h = node.dimensions[1];
  const p = [
    node.padding[0],
    node.padding[1],
    w - node.padding[2],
    h - node.padding[3]
  ];

  const active = node === node.parent.activeNode;

  ctx.fillStyle = active ? node.textColor : node.color;
  ctx.fillRect(0, 0, w, h);

  ctx.fillStyle = active ? node.color : node.textColor;
  if (node.text) {
    ctx.font = node.font;
    ctx.textAlign = node.textAlign ? node.textAlign : "left";
    ctx.save();
    ctx.translate(p[0], p[1]);
    ctx.fillText(node.text + (node.value ? node.value : ""), 0, 0, p[2]);
    ctx.restore();
  }
}

export function nodeTouched(game, node, np, p) {
  let hit = false;
  if (node.active) {
    if (typeof node.children === "object") {
      let i = 1;
      let lastPos = node.position;
      for (let key in node.children) {
        const child = node.children[key];
        if(child.active) {
          const x =
            child.position[0] +
            (node.stacking ? lastPos[0] : 0) +
            np[0] +
            node.position[0] +
            (node.stacking ? node.stackingOffset[0] * i : 0);
          const y =
            child.position[1] +
            (node.stacking ? lastPos[1] : 0) +
            np[1] +
            node.position[1] +
            (node.stacking ? node.stackingOffset[1] * i : 0);
          const w = x + child.dimensions[0];
          const h = y + child.dimensions[1];
          if (x <= p[0] && p[0] <= w && y <= p[1] && p[1] <= h) {
            game.gui.activeMenu.activeNode = child;
            const touchKey = child.actions.Touch;
            const touch = touchKey
              ? touchKey in actionHandlers
                ? actionHandlers[touchKey]
                : null
              : null;
            const enterKey = child.actions.Enter;
            const enter = enterKey
              ? enterKey in actionHandlers
                ? actionHandlers[enterKey]
                : null
              : null;
            touch ? touch(game) : enter ? enter(game) : null;
            if (
              typeof child.children === "object" &&
              Object.keys(child.children).length > 0
            ) {
              if(touch === null && enter === null) {
                return nodeTouched(game, child, [x, y], p);
              }
              else {
                hit = true;
              }
            } else {
              if(touch !== null || enter !== null) {
                hit = true;
              }
            }
          }
        }
        lastPos = [
          lastPos[0] + child.position[0],
          lastPos[1] + child.position[1]
        ];
        i++;
      }
    }
  }

  if(game.activeState === "menu")
    game.touches.shift();
  game.gui.children.hud.update = true;
  return hit;
}

export function drawNodeTree(ctx, node) {
  if (node.active || node.parent.active) {
    if (!node.parent.stacking) {
      ctx.save();
      ctx.translate(node.position[0], node.position[1]);
    } else {
      const x = node.parent.stackingOffset[0] + node.position[0];
      const y = node.parent.stackingOffset[1] + node.position[1];
      ctx.translate(x, y);
    }
    if (node.draw) {
      drawNode(ctx, node);
    }
    const children = Object.keys(node.children);
    if (children.length > 0 && typeof node.children === "object") {
      children.forEach(key => {
        drawNodeTree(ctx, node.children[key]);
      });
    }
    if (!node.parent.stacking) ctx.restore();
  }
}

export function createGui(tree, branch, comp) {
  const branches = Object.keys(tree);
  if (branches.length > 0) {
    branches.forEach(id => {
      branch.children[id] = { ...clone(comp), ...tree[id] };
      const node = branch.children[id];
      node.name = id;
      node.font = [node.fontWeight, node.fontSize + "px", node.fontFace].join(
        " "
      );

      if (branch.stacking) {
        node.textAlign = node.textAlign ? node.textAlign : branch.textAlign;
        node.dimensions[0] += branch.stackingDimensions[0];
        node.dimensions[1] += branch.stackingDimensions[1];
      }

      node.padding[0] += branch.padding[0];
      node.padding[1] += branch.padding[1];
      node.padding[2] += branch.padding[2];
      node.padding[3] += branch.padding[3];

      if (branch.activeNode && branch.activeNode === node.name) {
        branch.activeNode = node;
      }

      node.parent = branch;

      if (
        Object.keys(tree[id].children).length > 0 &&
        typeof tree[id].children === "object"
      ) {
        return createGui(tree[id].children, node, comp);
      }
    });
  } else {
    return (branch = clone(comp));
  }
  return branch;
}

export function initGui(game) {
  game.gui.activeMenu = game.gui.children.menu;
  game.gui.activeMenu.activeNode = game.gui.activeMenu.children.new;
}
