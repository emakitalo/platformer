"use strict";

import paths from "./paths.js";
import * as utils from "./utilities.js";
import * as twgl from "twgl-base.js";
import { m4, primitives } from "twgl.js";
import { getFramePosition } from "./utilities.js";

export function init(game) {
  const webgl = document.querySelector(".webgl");
  game.gl = webgl.getContext("webgl2")
    ? webgl.getContext("webgl2")
    : alert("WEBGL2 not supported");
  game.gl.canvas.style = "";
  game.ctx.canvas.style = "display: none;";
  game.ctx.canvas.width = game.width;
  game.ctx.canvas.height = game.height;
  game.fbo = primitives.createXYQuadBufferInfo(game.gl);

  const objs = game.assets.gameobjects;
  Object.keys(objs).forEach(obj => {
    const sprites = objs[obj].sprites;
    Object.keys(sprites).forEach(sprite => {
      const id = sprites[sprite].sprite;
      const img = game.assets.sprites[id];
      game.assets.textures[id] = twgl.createTexture(game.gl, {
        src: img
      });
    });
  });

  const shaders = [];
  Object.keys(game.assets.shaders).forEach(shader => {
    shaders.push(shader + "/" + shader + ".vs");
    shaders.push(shader + "/" + shader + ".fs");
  });

  return utils.getTexts(shaders, paths.shaders).then(shaders => {
    Object.keys(game.assets.shaders).forEach(shader => {
      game.assets.shaders[shader] = twgl.createProgramInfo(
        game.gl,
        shaders.splice(0, 2)
      );
    });
  });
}

function drawImage(
  gl,
  pInfo,
  bInfo,
  tex,
  targetWidth,
  targetHeight,
  texWidth,
  texHeight,
  srcX,
  srcY,
  srcWidth,
  srcHeight,
  dstX,
  dstY,
  dstWidth,
  dstHeight
) {
  gl.enable(gl.BLEND);
  if (srcWidth === undefined) {
    srcWidth = texWidth;
    srcHeight = texHeight;
  }

  if (dstX === undefined) {
    dstX = srcX;
    dstY = srcY;
    srcX = 0;
    srcY = 0;
  }

  if (dstWidth === undefined) {
    dstWidth = srcWidth;
    dstHeight = srcHeight;
  }

  var mat = m4.identity();
  var tmat = m4.identity();

  var uniforms = {
    u_matrix: mat,
    u_textureMatrix: tmat,
    u_texture: tex
  };
  // these adjust the unit quad to generate texture coordinates
  // to select part of the src texture

  // NOTE: no check is done that srcX + srcWidth go outside of the
  // texture or are in range in any way. Same for srcY + srcHeight

  m4.translate(tmat, [srcX / texWidth, srcY / texHeight, 0], tmat);
  m4.scale(tmat, [srcWidth / texWidth, srcHeight / texHeight, 1], tmat);

  // these convert from pixels to clip space
  m4.ortho(0, targetWidth, targetHeight, 0, -1, 1, mat);

  // these move and scale the unit quad into the size we want
  // in the target as pixels
  m4.translate(mat, [dstX, dstY, 0], mat);
  m4.scale(mat, [dstWidth, dstHeight, 1], mat);

  gl.useProgram(pInfo.program);
  twgl.setBuffersAndAttributes(gl, pInfo, bInfo);
  twgl.setUniforms(pInfo, uniforms);
  twgl.drawBufferInfo(gl, bInfo, gl.TRIANGLES);
}

export function draw(game) {
  const gl = game.gl;

  gl.viewport(0, 0, game.width, game.height);

  const objs = game.activeState.activeObjects;
  const order = game.activeState.stage.renderOrder;
  for (let i = 0; i < order.length; i++) {
    const obj = objs[order[i][0]];
    const state = obj.state[obj.state.length - 1];
    const action = obj.sprites[state];
    const frame = getFramePosition(obj, action);
    const id = action.sprite;
    const img = game.assets.sprites[id];

    drawImage(
      gl,
      game.assets.shaders.basic,
      game.fbo,
      game.assets.textures[id],
      game.width,
      game.height,
      img.width,
      img.height,
      frame[0],
      frame[1],
      action.frameSize,
      action.frameSize,
      Math.round(obj.position[0]),
      Math.round(obj.position[1]),
      action.frameSize * 0.5,
      action.frameSize * 0.5
    );
  }
}
