"use strict";

import "../scss/main.scss";
import globals from "./globals.js";
import paths from "./paths.js";
import * as utils from "./utilities.js";
import startGame from "./game.js";
import { createGui } from "./gui.js";

let WebGL = null;

if (globals.USE_WEBGL) {
  import("./webgl.js").then(m => {
    WebGL = m;
  });
}

const addVariable = new Map([
  [
    "vec2",
    () => {
      return [0, 0];
    }
  ]
]);

Number.prototype.mod = function(n) {
  return ((this%n)+n)%n;
};

Number.prototype.mapTo = function (in_min, in_max, out_min, out_max) {
  return ((this - in_min) * (out_max - out_min)) / (in_max - in_min) + out_min;
};

const container = document.querySelector(".game");
const canvas = document.querySelector(".canvas");
const gui = document.querySelector(".gui");

function loadAssetsAndStart(gameName) {
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  utils.getJSON(paths.game + gameName + ".json").then(game => {
    game.ctx = canvas.getContext("2d");
    game.ctxGui = gui.getContext("2d");
    game.ctxAudio = new AudioContext();
    game.svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    canvas.width = game.width;
    canvas.height = game.height;
    gui.width = game.width;
    gui.height = game.height;
    container.style.width = game.width + "px";
    container.style.height = game.height + "px";

    const volume = game.ctxAudio.createGain();
    volume.gain.value = game.volume;
    volume.connect(game.ctxAudio.destination);
    game.volume = volume;

    const levels = utils.getJSONs(game.levels, paths.levels);
    const gameobjects = utils.getJSONs(
      game.assets.gameobjects,
      paths.gameobjects
    );

    Promise.all([levels, gameobjects])
      .then(data => {
        const sounds = [];
        const component = utils.getJSON(paths.menus + "component.json");

        Object.keys(game.levels).forEach(level => {
          game.levels[level] = data[0].shift();
          game.levels[level].stage.background.forEach(bg => {
            bg.position[0] = -50;
          })
        });

        Object.keys(game.assets.gameobjects).forEach(oId => {
          const gObj = data[1].shift();
          Object.keys(gObj).forEach(id => {
            const value = gObj[id];
            if (addVariable.has(value)) {
              gObj[id] = addVariable.get(value)();
            }
          });

          game.assets.gameobjects[oId] = gObj;

          // Get gameobject sounds
          Object.keys(gObj.sounds).forEach(sndId => {
            sounds.push(
              utils.getAudio(paths.sounds + oId + "/" + sndId + ".mp3")
            );
          });
        });
        return Promise.all([Promise.all(sounds), component]);
      })
      .then(data => {
        Object.keys(game.assets.gameobjects).forEach(oId => {
          const obj = game.assets.gameobjects[oId];
          Object.keys(obj.sprites).forEach(sId => {
            let action = obj.sprites[sId];
            if (action.sprite === null) {
              const s = [];
              game.assets.sprites.push(s);
              s.push(
                utils.getImage(
                  paths.sprites + oId + "/" + sId + ".png",
                  img => {
                    const len = action.columns * action.rows;
                    const collisions = [];
                    for (let i = 0; i < len; i++) {
                      action.frame = i;
                      const fp = utils.getFramePosition(1, action);
                      const data = utils.getData(
                        game.ctx,
                        img,
                        fp,
                        action.frameSize
                      );
                      collisions.push(utils.getOutline2(data.data, action.frameSize));
                    }
                    action.frame = 0;
                    game.ctx.canvas.width = game.width;
                    game.ctx.canvas.height = game.height;
                    s[0] = img;
                    s[2] = collisions;
                  }
                )
              );
              s.push(
                utils.getImage(
                  paths.sprites + oId + "/" + sId + "_reversed.png",
                  img => {
                    const len = action.columns * action.rows;
                    const collisions = [];
                    for (let i = 0; i < len; i++) {
                      action.frame = i;
                      const fp = utils.getFramePosition(-1, action);
                      const data = utils.getData(
                        game.ctx,
                        img,
                        fp,
                        action.frameSize
                      );
                      collisions.push(utils.getOutline2(data.data, action.frameSize));
                    }
                    action.frame = 0;
                    game.ctx.canvas.width = game.width;
                    game.ctx.canvas.height = game.height;
                    s[1] = img;
                    s[3] = collisions;
                  }
                )
              );
              action.sprite = game.assets.sprites.length - 1;
            } else {
              const otherAction = obj.sprites[action.sprite];
              action = { ...otherAction, ...action };
              action.sprite = otherAction.sprite;
            }
            action.duration = action.stop - action.start;
            action.timerOffset =
              game.ups / action.fps / Math.abs(action.duration);
            obj.sprites[sId] = action;
          });

          Object.keys(obj.sounds).forEach(sndId => {
            game.ctxAudio.decodeAudioData(data[0].shift(), audio => {
              game.assets.sounds.push(audio);
              obj.sounds[sndId] = game.assets.sounds.length - 1;
            });
          });
        });

        Object.keys(game.assets.images).forEach(imgId => {
          game.assets.images[imgId] = utils.getImage(
            paths.images + imgId + ".png",
            img => {
              const data = utils.getData(
                game.ctx,
                img,
                [0,0],
                [img.width, img.height]
              );
              game.ctx.canvas.width = game.width;
              game.ctx.canvas.height = game.height;
              game.assets.images[imgId] = [
                game.ctx.createPattern(img, "repeat"),
                utils.getOutline2(data.data, [img.width, img.height], false),
                [img.width, img.height]
              ];
            }
          );
        });

        const tree = utils.clone(data[1]);
        createGui(game.gui.children, tree, data[1]);
        game.gui = tree;
      })
      .then(() => {
        utils.resize(container);
        WebGL
          ? WebGL.init(game).then(() => {
              startGame(game);
            })
          : (() => {
              startGame(game);
            })();
      });
  });
}

window.onresize = () => {
  utils.resize(container);
};

window.addEventListener("load", () => {
  if (canvas.getContext) {
    loadAssetsAndStart("game");
  } else {
    alert("Canvas not supported!");
  }
});
