"use strict";

import { tabActive } from "./utilities.js";
import actionHandlers from "./actions.js";
import { initTouch } from "./touch.js";
import { nodeTouched } from "./gui";

const moveTreshold = 20;
const dirTreshold = 10;

const keyActions = [
  "jump",
  "attack",
  "moveLeft",
  "moveRight",
  "moveUp",
  "moveDown"
];

export const keyToAction = {
  p: "p",
  a: "a",
  r: "r",
  d: "d",
  Enter: "Enter",
  Escape: "Escape",
  " ": keyActions[0],
  x: keyActions[1],
  ArrowLeft: keyActions[2],
  ArrowRight: keyActions[3],
  ArrowUp: keyActions[4],
  ArrowDown: keyActions[5]
};

const menuActions = {
  moveUp: game => {
    actionHandlers.moveInMenu(game, [-1, 0]);
  },
  moveDown: game => {
    actionHandlers.moveInMenu(game, [1, 0]);
  },
  moveLeft: game => {
    const node = game.gui.activeMenu.activeNode;
    const name = node.actions.moveLeft;
    const action = actionHandlers[name];
    if (action) {
      if (name === "changeVolume") action(game, -1);
      else action(game);
    }
  },
  moveRight: game => {
    const node = game.gui.activeMenu.activeNode;
    const name = node.actions.moveRight;
    const action = actionHandlers[name];
    if (action) {
      if (name === "changeVolume") action(game, 1);
      else action(game);
    }
  },
  Enter: game => {
    const node = game.gui.activeMenu.activeNode;
    const name = node.actions.Enter;
    const action = actionHandlers[name];
    if (action) {
      action(game);
    }
  }
};

export function initInput(game) {
  initTouch(game);
  tabActive(() => {
    actionHandlers.pause(game);
  });

  document.addEventListener(
    "keydown",
    e => {
      if (!game.inputs.includes(keyToAction[e.key]))
        game.inputs.push(keyToAction[e.key]);
    },
    false
  );

  document.addEventListener(
    "keyup",
    e => {
      if (game.inputs.includes(keyToAction[e.key]))
        game.inputs.splice(game.inputs.indexOf(keyToAction[e.key]), 1);
    },
    false
  );
}

function updateGameplay(game) {
  const player = game.activeLevel.player;
  const playerW = player.sprites.static.frameSize[0] * 0.5;
  const playerH = player.sprites.static.frameSize[1] * 0.5;
  const playerX = player.position[0] + playerW;
  const playerY = player.position[1] + playerH;
  const actions = player.activeActions;
  let inputs = game.inputs;

  if (game.touches.length > 0) {
    const rect = game.ctx.canvas.getBoundingClientRect();
    for(let i = 0; i < game.touches.length; i++) {
      const touch = game.touches[i];
      const x = touch.clientX;
      const y = touch.clientY;
      const posX = Math.round(x.mapTo(rect.left, rect.left + rect.width, 0, game.width));
      const posY = Math.round(y.mapTo(rect.top, rect.top + rect.height, 0, game.height));

      if(!nodeTouched(game, game.gui.activeMenu, [0, 0], [posX, posY])) {
        const al = inputs.indexOf(keyToAction["ArrowLeft"]);
        const ar = inputs.indexOf(keyToAction["ArrowRight"]);

        if(posX < playerX && al < 0) {
          if(ar > -1) 
            inputs.splice(ar, 1);
          inputs.push(keyToAction["ArrowLeft"]);
        } else if(posX > playerX && ar < 0) {
          if(al > -1) 
            inputs.splice(al, 1);
          inputs.push(keyToAction["ArrowRight"]);
        } else {
          if(Math.abs(posX - playerX) < moveTreshold) {
            if(ar > -1) 
              inputs.splice(ar, 1);
            if(al > -1) 
              inputs.splice(al, 1);
          }
        }

        if(!actions.includes("Jump")) {
          const au = inputs.indexOf(keyToAction["ArrowUp"]);
          const ad = inputs.indexOf(keyToAction["ArrowDown"]);

          if(posY < playerY && au < 0) {
            if(ad > -1) 
              inputs.splice(ad, 1);
            inputs.push(keyToAction["ArrowUp"]);
          } else if(posY > playerY && ad < 0) {
            if(au > -1) 
              inputs.splice(au, 1);
            inputs.push(keyToAction["ArrowDown"]);
          } else {
            if(Math.abs(posY - playerY) < moveTreshold) {
              if(ad > -1) 
                inputs.splice(ad, 1);
              if(au > -1) 
                inputs.splice(au, 1);
            }
          }
        }
      } else {
        game.touches.shift();
      }
    }
  }

  for (let i = 0; i < actions.length; i++) {
    const action = actions[i];
    const inKeys = keyActions.includes(action);
    const inInputs = !inputs.includes(action) && inKeys;
    if (inInputs) {
      const actionId = game.activeLevel.player.state.indexOf(action);
      const sprite = game.activeLevel.player.sprites[action];
      if (actionId > 0 && !sprite.pingpong) {
        sprite.active = false;
        game.activeLevel.player.state.splice(actionId, 1);
      } else {
        if (sprite.pingpong && !sprite.ponged) {
          sprite.pinged = true;
        }
      }
    }
  }

  for (let i = 0; i < inputs.length; i++) {
    const action = inputs[i];
    if (game.activeLevel.player.active) {
      if (!actions.includes(action)) {
        if (actionHandlers[action]) {
          const sprite = game.activeLevel.player.sprites[action];
          actions.push(action);
          if (
            sprite.overrides
              ? !sprite.overrides.includes(
                  game.activeLevel.player.state[
                    game.activeLevel.player.state.length - 1
                  ]
                )
              : false
          ) {
            game.activeLevel.player.state.push(action);
          } else {
            game.activeLevel.player.state.splice(
              game.activeLevel.player.state.length - 1,
              0,
              action
            );
          }
        } else {
          console.log(action + " does not exist!");
        }
      }
    }

    if (action === "r") {
      game.activeLevel.player.position[0] = 20;
      game.activeLevel.player.position[1] = game.height * 0.5;
      game.inputs.splice(game.inputs.indexOf(action), 1);
    }

    if (action === "a") {
      const id = Math.floor(Math.random() * game.activeLevel.enemies.length);
      const enemy = game.activeLevel.enemies[id];
      const params = {
        position: [
          game.width,
          (Math.random() * game.height).mapTo(
            0,
            game.height,
            50,
            game.height - 50
          )
        ],
        type: "enemy",
        direction: [-1, 1],
        velocity: [0, 1],
        speedX: 1 + Math.random().mapTo(0, 1, 0, 0.5)
      };
      actionHandlers.spawn(game, enemy, ["moveX", "moveSineY"], params);
      game.inputs.splice(game.inputs.indexOf(action), 1);
    }

    if (action === "Escape") {
      actionHandlers.showMenu(game);
      game.inputs.splice(game.inputs.indexOf(action), 1);
    }
  }
}

function updateMenu(game) {
  if (game.touches.length > 0) {
    const rect = game.ctxGui.canvas.getBoundingClientRect();
    const x = game.touches[0].clientX.mapTo(
      rect.left,
      rect.right,
      0,
      game.width
    );
    const y = game.touches[0].clientY.mapTo(
      rect.top,
      rect.bottom,
      0,
      game.height
    );
    nodeTouched(game, game.gui.activeMenu, [0, 0], [x, y]);
  }
  const inputs = game.inputs;
  for (let i = 0; i < inputs.length; i++) {
    const key = inputs[i];
    if (game.inputs.includes(key)) {
      if (menuActions[key]) {
        menuActions[key](game);
      }
      game.inputs.splice(game.inputs.indexOf(key), 1);
    }
  }
}

const states = {
  gameplay: updateGameplay,
  menu: updateMenu
};

export function updateInput(game) {
  game.inputLock = true;
  states[game.activeState](game);
  game.inputLock = false;
}
