"use strict";

import globals from "./globals.js";
import { getFramePosition } from "./utilities.js";
import * as Gui from "./gui.js";

let WebGL = null;

if (globals.USE_WEBGL) {
  import("./webgl.js").then(m => {
    WebGL = m;
  });
}

function drawHud(game) {
  const hud = game.gui.children.hud;
  if(hud.active) {
    if (hud.update) {
      Gui.drawNodeTree(game.ctxGui, hud);
      hud.update = false;
    }
  } else {
    game.ctxGui.clearRect(
      hud.position[0],
      hud.position[1],
      hud.dimensions[0],
      hud.dimensions[1]
    );
  }
}

function drawGameplay(game) {
  const bgs = game.assets.images;
  const menu = game.gui.children.menu;

  if(menu.clear) {
    game.ctxGui.clearRect(menu.position[0], menu.position[1], menu.dimensions[0], menu.dimensions[1]);
    menu.clear = false;
    menu.draw = true;
  }

  game.ctx.clearRect(0, 0, game.width, game.height);

  game.activeLevel.stage.background.forEach(bg => {
    var matrix = game.svg.createSVGMatrix();
    matrix.e = Math.round(-bg.position[0]);
    matrix.f = Math.round(bg.position[1]);

    const img = bgs[bg.image][0];
    img.setTransform(matrix);
    game.ctx.fillStyle = img;
    game.ctx.fillRect(0, 0, game.width, game.height);

    /*
    const cb = bgs[bg.image][1];
    for(let i=0; i < cb.length; i+=2) {
      const x = Math.round(cb[i] + bg.position[0]);
      const y = Math.round(cb[i+1] + bg.position[1]);
      game.ctx.fillStyle = '#0f0';
      game.ctx.fillRect(x, y, 1, 1);
    }
    */
  });

  const objs = game.activeLevel.activeObjects;
  const order = game.activeLevel.stage.renderOrder;
  for (let i = 0; i < order.length; i++) {
    const obj = objs[order[i][0]];
    const state = obj.state[obj.state.length - 1];
    const action = obj.sprites[state];
    const frame = getFramePosition(obj.direction[0] > 0 ? 1 : -1, action);
    const sprites = game.assets.sprites[action.sprite];
    const sprite = obj.direction[0] > 0 ? sprites[0] : sprites[1];

    game.ctx.drawImage(
      sprite,
      frame[0],
      frame[1],
      action.frameSize[0],
      action.frameSize[1],
      Math.round(obj.position[0]),
      Math.round(obj.position[1]),
      action.frameSize[0],
      action.frameSize[1]
    );

    /*
    const cb = obj.direction[0] > 0 ? sprites[2][action.frame] : sprites[3][action.frame];
    for(let i=0; i < cb.length; i+=2) {
      const x = Math.round(cb[i] + obj.position[0]);
      const y = Math.round(cb[i+1] + obj.position[1]);
      game.ctx.fillStyle = '#0f0';
      game.ctx.fillRect(x, y, 1, 1);
    }
    */
  }

  game.activeLevel.stage.foreground.forEach(fg => {
    var matrix = game.svg.createSVGMatrix();
    matrix.e = Math.round(fg.position[0]);
    matrix.f = Math.round(fg.position[1]);

    const img = bgs[fg.image][0];
    img.setTransform(matrix);
    game.ctx.fillStyle = img;
    game.ctx.fillRect(0, 0, game.width, game.height);
  });

  drawHud(game);
}

function drawMenu(game) {
  if(game.activeLevel) {
    if(game.activeLevel.clear) {
      const hud = game.gui.children.hud;
      game.ctxGui.clearRect(0, 0, hud.dimensions[0], hud.dimensions[1]);
      game.ctx.clearRect(0, 0, game.width, game.height);
      game.activeLevel.clear = false;
    }
  }

  const menu = game.gui.activeMenu;
  if(menu.active) {
    if(menu.draw) {
      Gui.drawNodeTree(game.ctxGui, menu);
      menu.draw = false;
    }
  } else {
    game.ctxGui.clearRect(
      menu.position[0],
      menu.position[1],
      menu.dimensions[0],
      menu.dimensions[1]
    );
  }
}

const states = {
  gameplay: drawGameplay,
  menu: drawMenu
};

export function draw(game) {
  if (WebGL) {
    WebGL.draw(game);
  } else {
    states[game.activeState](game);
  }
}
