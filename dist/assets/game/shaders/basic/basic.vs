attribute vec4 position;
attribute vec2 texcoord;

uniform mat4 u_matrix;
uniform mat4 u_textureMatrix;

varying vec2 v_texcoord;

void main () {
  v_texcoord = (u_textureMatrix * vec4(texcoord,0,1)).xy;
  gl_Position = u_matrix * position;
}